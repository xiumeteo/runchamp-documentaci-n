﻿APROBADOS:
	Plan de Comunicación 1.1
	Proceso de Administración de Riesgos 1.0
	Proceso de Administración de Requisitos 1.0
	Proceso de ACS 1.0

ENTREGADOS:
	Proceso de QA 1.0
	Proceso de Medición y Análisis 1.0
	Plan de ACS 1.1
	Proceso de Control 1.0
	Proceso de Planeación 1.0


PENDIENTES:
	Plan de Desarrollo del Proyecto
	EDT y Agenda
	Reporte de Estado del Proyecto
	Plan de Riesgos 
	Plan de Medición y Análisis
	Plan de Control
	Plan de QA



--------------------------------------------------------
				LISTA DE ENTREGAS
--------------------------------------------------------

ENTREGA 29-11-2013
	EDT y Agenda 1.0
	Proceso de Planeación 1.0.1
	Plan de Riesgos 1.0
	Plan de Medición y Análisis 1.0
	Plan de Control 1.0
	Plan de QA 1.0


ENTREGA 27-11-2013
	Proceso de Control 1.0

ENTREGA 26-11-2013
	Proceso de ACS 1.1
	Proceso de QA 1.0
	Proceso de Medición y Análisis 1.0

ENTREGAS PREVIAS (NO SE REGISTRÓ LA FECHA):
	Plan de Comunicación 1.1
	Proceso de Administración de Riesgos 1.0
	Proceso de Administración de Requisitos 1.0